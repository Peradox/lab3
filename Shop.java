import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
		Album[] albums= new Album[4];
		Scanner scan= new Scanner(System.in);
		
		for(int i=0;i<albums.length;i++)
		{
			albums[i]= new Album();
			System.out.println(" ");
			System.out.println("Album #"+(i+1));
			System.out.println("What is the album name?");
			albums[i].name=scan.nextLine();
			System.out.println("Who is the artist?");
			albums[i].artist=scan.nextLine();
			System.out.println("What format is it in? e.g. cassette");
			albums[i].format=scan.nextLine();
		}
		
		System.out.println("Album name: "+albums[3].name);
		System.out.println("Artist: "+albums[3].artist);
		System.out.println("Format: "+albums[3].format);
		
		albums[3].musicPlayer();
		
	}
	
}