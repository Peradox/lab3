public class Album{
	public String name;
	public String artist;
	public String format;
	
	public void musicPlayer(){
		format=this.format.toLowerCase();
		System.out.println("You will need a "+format+" player to play this album");
	}
}